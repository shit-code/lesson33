package dev.abnormally.alevel.impossible;

import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public final class Application {
    private final static HashMap<String, String> questions;
    private final static Scanner reader = new Scanner(System.in);
    private static Bomb bomb;
    private static Thread ticker;

    public static void main(String[] args) {
        String chosen = (String) questions.keySet().toArray()[new Random().nextInt(questions.size())];
        ticker = new Thread((bomb = new Bomb(chosen, questions.get(chosen))));

        System.out.println("You have only 10 seconds!");
        System.out.println(bomb.getQuestion());
        ticker.start();

        while (!bomb.isToLate()) {
            if (reader.nextLine().toLowerCase().trim().equals(bomb.getAnswer().toLowerCase()) && !bomb.isToLate()) {
                bomb.setAnswered(true);
                ticker.interrupt();
            }

            if (bomb.isToLate() && !bomb.isAnswered()) {
                System.out.println("Sorry, partner, you already die.");
            }
        }
    }

    private static class Bomb implements Runnable {
        private final String question;
        private final String answer;

        private boolean toLate;
        private boolean answered;
        private int seconds = 10;

        Bomb(String question, String answer) {
            setToLate(false);
            setAnswered(false);

            this.question = question;
            this.answer = answer;
        }

        @Override
        public void run() {
            while (seconds != 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("You stopped it!");
                    System.exit(0);
                }

                seconds--;
            }

            if (!isAnswered()) {
                setToLate(true);
                System.out.println("Boom");
                System.exit(0);
            } else {
                System.out.println("You stopped it!");
            }
        }

        public synchronized void setAnswered(boolean answered) {
            this.answered = answered;
        }

        public boolean isAnswered() {
            return answered;
        }

        private synchronized void setToLate(boolean late) {
            toLate = late;
        }

        public boolean isToLate() {
            return toLate;
        }

        public String getQuestion() {
            return question;
        }

        public String getAnswer() {
            return answer;
        }

    }

    static {
        questions = new HashMap<>();

        questions.put("Why?", "Because");
        questions.put("Where?", "There");
        questions.put("When?", "Today");
    }

}
